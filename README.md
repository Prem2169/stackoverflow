# stackoverflow

This is the mini-project made in laravel for practice

**Some Screenshot of project :**

**Login :** 

![](screenshots/login.png)

**Questions/home page :**

![](screenshots/questions-home.png)

**Ask a Question / Edit Question :**

![](screenshots/ask-or-edit-question.png)

**Question Posted Successfully :**

![](screenshots/question-added-successfully.png)

**After Clicking a Question :**

![](screenshots/question-show.png)

**Replying to Question :**
Replying from other user's account :)
![](screenshots/reply-question.png)
![](screenshots/reply-question-success-1.png)
![](screenshots/reply-question-success-2.png)

**Notification :**
- As a reply was added, so the question's author will be notified

![](screenshots/notification-1.png)
![](screenshots/notification-2.png)

**Mark Question as Favorite :**
 **Note :** 
 - You can't mark favorite to your own question 
 - click to the 'star' icon to mark question as favorite

![](screenshots/mark-as-favorite.png)

**Upvote/Downvote  to  Quesiton/Answer :**
- You can upvote/downvote by clicking to the up arrow or down arrow

![](screenshots/upvote.png)

**Mark as Best Answer :**
**Note :**
- The Author of the question can only mark any answer as best answer
- Only one answer can become the best answer of the that question

- Before Marking as Best answer :

![](screenshots/before-mark-as-best-answer.png)

- After Marking as Best answer :

![](screenshots/after-mark-as-best-answer.png)



