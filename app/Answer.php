<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $guarded = [];


    /**
     * Eloquent Event Handling
     * To use the event we have to use the one Model's mehtod, or we have to override the Model's class Method
     * the method is 'boot()' method === See the code below
     */
    public static function boot()
    {
        //to do the work of the parent class
        parent::boot();

        /**
         * Syntax :
         * static::EventName(function($object){
         *      //do your work here
         * })
         *
         */
        static::created(function($answer){
            //Here we will automatically get the Answer's Object which is created, (mtlb jbi bhi humara answer created ho jayega uske baad ye method call hoyega)
            $answer->question()->increment('answers_count');
        });
        static::deleted(function($answer){
            //Here we will automatically get the Answer's Object which is created, (mtlb jbi bhi humara answer created ho jayega uske baad ye method call hoyega)
            $answer->question()->decrement('answers_count');
        });
    }

    public function getCreatedDateAttribute()
    {
        // dd($this->created_at->diffForHumans());
        return $this->created_at->diffForHumans();
    }
    public function getBestAnswerStatusAttribute()
    {
        if($this->id === $this->question->best_answer_id)
        {
            return 'text-success';
        }
        return 'text-dark';
    }
    public function getIsBestAttribute()
    {
        return $this->id === $this->question->best_answer_id;
    }

    /**
     * RELATIONSHIP METHODS
     */
    public function question(){
        return $this->belongsTo(Question::class);
    }
    public function author(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function votes(){
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }

    /**
     * HELPER FUNCTIONs
     */
    public function vote(int $vote){
        $this->votes()->attach(auth()->id(), ['vote'=>$vote]);
        if($vote < 0)
        {
            $this->decrement('votes_count');
        }
        else
        {
            $this->increment('votes_count');
        }
    }
    public function updateVote(int $vote)
    {
        //User may have already up-voted this answer and now down votes (votes_count = 10) and now user down votes (votes_count = 9) votes_count = 8
        if($vote < 0)
        {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        }
        else
        {
            $this->increment('votes_count');
            $this->increment('votes_count');
        }
    }

}
