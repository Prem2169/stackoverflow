<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->only(['create','store', 'edit', 'update']);
    }

    public function index()
    {
        $questions = Question::with('owner')->latest('created_at')->paginate(10);//'Eager Loading'
        /**
         * 'Eager Loading'
         * In the above line we have used the 'with()' method
         * this is called 'Eager Loading'
         * this is done bcoz while displaying these 10 records which paginate gave to us, so udr vo 10 baar dikhane ke liye 10 baar owner leke aa rha h, is problem ko 'n+1 query' prblm bolte h, so udr performance jaa rhi h, so to overcome this problem we have used 'eager loading'
         * idr actually kya karega, 'with()' method apne aap 'owner' wala relation bind karega question ke sath,
         * so vo kya karega : phele paginate se questions layega, and then un questions m se saare user_id lekr aayega, and jo jo user)id aaye, toh un user_id ke query maarke , ek sath users(owner) lekr aayega
         *
         */
        return view('questions.index', compact([
            'questions'
        ]));
    }


    public function create()
    {
        app('debugbar')->disable();
        return view('questions.create');
    }

    public function store(CreateQuestionRequest $request)
    {
        auth()->user()->questions()->create([
            'title' => $request->title,
            'body' => $request->body
        ]);
        Session()->flash('success', "Question has been added successfully!");
        return redirect(route('questions.index'));
    }


    // public function show($slug)
    // {
    //     $question = Question::where('slug', $slug)->firstOrFail();
    // }
    /**
     * as we have used except() in the route, we have seperated show from the resource, and manually created the show ka route and gave the slug, as we gave the slug the above commented wala code is fine, but how beautiful if, it should bind with the slug also,
     * as all the ModelBindings are done defaultly with id, but this only one show method ko slug se bind krna h , toh kaise kare,
     * so we have used 'RouteServiceProvider'
     *  :   iske andar boot() method m humne slug se kaise bind kare vo likha h
     *  :   watch the app/providers/RouteServiceProvider.php dekho
     */
    public function show(Question $question)
    {
        $question->increment('views_count');
        return view('questions.show', compact([
            'question'
        ]));
    }

    public function edit(Question $question)
    {
        /**
         * for gates and policies documentation, see the AuthServiceProvider file as well as laravel-concepts file
         * these all are authorization so gates and policies are mainly used if the user can edit or delte any post or question or anything, the main attention here is that, is he authorized to edit or delete smja?
         *
         */


        // Using Gates
        /**
         * Here can() method of user will automatically checks from the gates we define and matches it and runs it
         */
        // if(auth()->user()->can('update-question', $question))
        // if(Gate::allows('update-question', $question))
        // {
        //     app('debugbar')->disable();
        //     return view('questions.edit', compact([
        //         'question'
        //     ]));
        // }
        // abort(403);


        // Using Policy
        if($this->authorize('update', $question))
        {
            app('debugbar')->disable();
            return view('questions.edit', compact([
                'question'
            ]));
        }
        abort(403);
    }

    public function update(UpdateQuestionRequest $request, Question $question)
    {
        //Using GATES
        // if(Gate::allows('update-question', $question))
        // {
        //     $question->update([
        //         'title'=>$request->title,
        //         'body'=>$request->body
        //     ]);
        //     Session()->flash('success', "Question has been Updated successfully!");
        //     return redirect(route('questions.index'));
        // }
        // abort(403);

        //Using Policy
        if($this->authorize('update', $question))
        {
            $question->update([
                'title'=>$request->title,
                'body'=>$request->body
            ]);
            Session()->flash('success', "Question has been Updated successfully!");
            return redirect(route('questions.index'));
        }
        abort(403);
    }

    public function destroy(Question $question)
    {
        // Using GATES
        // if(auth()->user()->can('delete-question', $question))
        // {
        //     $question->delete();
        //     Session()->flash('success', "Question has been Deleted successfully!");
        //     return redirect(route('questions.index'));
        // }
        // abort(403);

        //Using POLICY
        if($this->authorize('delete', $question))
        {
            $question->delete();
            Session()->flash('success', "Question has been Deleted successfully!");
            return redirect(route('questions.index'));
        }
        abort(403);
    }
}
