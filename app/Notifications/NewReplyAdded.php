<?php

namespace App\Notifications;

use App\Question;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewReplyAdded extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( Question $question)
    {
        $this->question = $question;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //Here by default only mail is selected, so if we wrote the database after mail, then the notifications are stored in database
        //But for that there should be database for that, so laravel gives that automatically, run command 'php artisan notifications:table' this command will create one migration and then you have to migrate that migration
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    // This function is used to send the notification in mail
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('A new reply has been added to your question.')
                    ->action('View Question', url($this->question->url))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    //This method is used to store the notification inside the database
    public function toArray($notifiable)
    {
        return [
            'question'=>$this->question
        ];
    }
}
