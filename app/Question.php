<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Question extends Model
{
    //
    protected $guarded = [];

    /**
     * MUTATORS -
     * These are special functions which have setXXXAttribute($value)
     * the code of this setXXXAttribute is : for eg :
     *      {
     *          public function setTitleAttribute($value)
     *          {
     *              $this->attributes['title'] = $value;
     *          }
     *      }
     * This above method is called when we set the value of the title
     * like: $question->title = "What is static in java";
     * so for the above line the method we wrote will be called
     * These mutators are helpful when we want some action or some code to be run when we set the vlue of any attribute
     *
     *
     * in our example we want to set the slug attribute whenever the title is set so when ever we will set the title then slug will automatically be created and added to it
     */
    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }

    /**
     * ACCESSORS - These are special functions which have getXXXAttribute()
     * the code of this getXXXAttribute is : for eg :
     *      {
     *          public function getUrlAttribute()
     *          {
     *              return "question/{$this->id}"
     *          }
     *      }
     * This above method is called when we want or want to get the value of the url
     * like: $question->url
     * so for the above line the method we wrote will be called
     * These 'accessors' are helpful when we want some action or some code to be run when we want to get the vlue of any attribute
     *
     * * one short example is : when we want the first_name and last_name to be concatinated and return : so we will make one accessor mehtod like getFullNameAttribute()
     * Inside this accessor method we will return the concatinated name so here we easily got the full name
     * when we use 'anyUser->full_name' : the above accessor will be called
     */
    public function getUrlAttribute()
    {
        // return "questions/{$this->id}";
        return "questions/{$this->slug}";
    }
    public function getCreatedDateAttribute()
    {
        // dd($this->created_at->diffForHumans());
        return $this->created_at->diffForHumans();
    }
    public function getAnswersStyleAttribute(){
        if($this->answers_count>0){
            if($this->best_answer_id)
            {
                return 'has-best-answer';
            }
            return 'answered';
        }
        return 'unanswered';
    }
    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }
    public function getIsFavoriteAttribute()
    {
        return $this->favorites()->where(['user_id'=>auth()->id()])->count() > 0;
    }



    /**
     * Relation ships mehthods
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
    public function favorites()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
    public function votes(){
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }

    /**
     * HELPER FUNCTION
     */
    public function markBestAnswer($answer)
    {
        $this->best_answer_id = $answer->id;
        $this->save();
    }
    public function vote(int $vote){
        $this->votes()->attach(auth()->id(), ['vote'=>$vote]);
        if($vote < 0)
        {
            $this->decrement('votes_count');
        }
        else
        {
            $this->increment('votes_count');
        }
    }
    public function updateVote(int $vote)
    {
        //User may have already up-voted this question and now down votes (votes_count = 10) and now user down votes (votes_count = 9) votes_count = 8
        if($vote < 0)
        {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        }
        else
        {
            $this->increment('votes_count');
            $this->increment('votes_count');
        }
    }
}
