<?php

namespace App\Providers;

use App\Answer;
use App\Policies\AnswerPolicy;
use App\Policies\QuestionsPolicy;
use App\Question;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Question::class => QuestionsPolicy::class,
        Answer::class => AnswerPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // GATES
        /**
         * This is gate, gate is for authorization, bcoz not every users, can edit or delete the questions, mtlb, edit aur delete button usi questions pe aane chahaye, if the user jo loged in h, and uska he questions dashboard pe dikha rha h, toh he edit and delete button dikhaye
         * Note: idr jo function ke andar '$user' h , laravel apne aap authorized user idr le le ga, humko bus usko 'question' dena h, and it will return true or false , based on if the user is the same as the author of the question
         */
        // Gate::define('update-question', function($user, $question){
        //     return $user->id === $question->user_id;
        // });
        // Gate::define('delete-question', function($user, $question){
        //     return $user->id === $question->user_id  && $question->answers_count === 0;
        // });

//OR

        // POLICIES
        /**
         * SEE app/policies/QuestionsPolicy
         */
    }
}
