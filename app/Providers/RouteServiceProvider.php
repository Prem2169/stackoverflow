<?php

namespace App\Providers;

use App\Question;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * as we want the 'show' route of the question to be binded by the 'slug', by default the binding(Model Binding) is with 'id' , so if we want only the show route of the question to be binded by the 'slug', this is the code written below
         * One condition can occur i.e
         *      what if idr toh merko questions.show ko he slug se lana h, what if future m koi aur model ka show bhi slug ke lana ho, for eg humko answers ka bhi sirf show wala route slug se chaheye toh hume idr Route::bind('slug', callback) ye wale ke andar ek if dala padega, and us if ke andar hum 'route()->has' wala method use krna padega and dekhna hoga ki ye slug quesitons ka h ki answers ya koi aur model ka h(id created) toh uss hisab se us model ka slug return krwana padega
         */
        Route::bind('slug', function($slug){
            return Question::with('answers.author')->where('slug', $slug)->firstOrFail();
        });
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
