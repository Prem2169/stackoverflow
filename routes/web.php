<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


/**
 * Here we have used except() method in Route::resource bcoz we want the show() of the question not to be binded by the id, we want the model binding of the show to be bind wiht the slug, so thats why we have used except(), this method will not create the show() in the resource, so we have to manually create the route for the show() abd we have done that here
 */
Route::get('users/notification', 'UsersController@notifications')->name('users.notifications');

Route::resource('questions', 'QuestionsController')->except('show');
Route::get('questions/{slug}', 'QuestionsController@show')->name('questions.show');


Route::post('question/{question}/favorite', 'FavoritesController@store')->name('questions.favorite');
Route::delete('question/{question}/unfavorite', 'FavoritesController@destroy')->name('questions.unfavorite');

Route::post('question/{question}/vote/{vote}', 'VotesController@voteQuestion')->name('questions.vote');
Route::post('answer/{answer}/vote/{vote}', 'VotesController@voteAnswer')->name('answers.vote');

/**
 * Here we r making one resourceful routes of the answer, but note that here answers is not the independent route, it is dependent to the question, mtlb ki is question ka ye answer, aaisa, so we will not see any answer alone like question so see how to make dependent routes
 */
Route::resource('questions.answers', 'AnswersController')->except(['index', 'show', 'create']);
/**
 * Here questions.answers likha h so it will automatically creates the routes for it, see in the route:list how the routes are defined
 */


Route::put('answers/{answer}/best-answer', 'AnswersController@bestAnswer')->name('answers.bestAnswer');
Route::get('/home', 'HomeController@index')->name('home');
