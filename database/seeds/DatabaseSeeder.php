<?php

use App\Answer;
use App\Question;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        factory(\App\User::class, 5)->create()->each(function($user){
            //this body will run for every user

            //BEFORE ANSWERS WERE MADE
            // for($i=1; $i<=rand(5,10); $i++)
            // {
            //     $user->questions()->create(factory(\App\Question::class)->make()->toArray());
            //     /**
            //      * Here from the above line $users->questions()->create() <- so jo ye create h na model wala isko array chahaye rehta h,
            //      * mtlb ki ek record ka array and vo record create mtlb db m daal dega so humko array aaye kaha se , so humne question ka factory phele se he bna ke rkha tha , so we use here, so here 'make()' method will make the object of the question class and return it, what? it will return the object of Question class but we want 'array' as create() method ko array chahaye rehta h, so islsye humne toArray() method use kia, thats convert the object to array
            //      *
            //      * This 'make()' method is of factory, it will automatically generate the questions as we wrote in QuestionFactory, and the make will create an object of this and will store the data inside that object and then return to it, and that returned object ko hum array m convert krke bhej rahe h , smja?
            //      */

            //     /**
            //      * To run this 'seed' the command is :
            //      *        'php artisan db:seed'
            //      * To run this 'seed' with the migrate:fresh the command is :
            //      *        'php artisan migrate:fresh --seed'
            //      *
            //     */

            // }

            //AFTER CREATING ANSWER AND ANSWERSFACTORY, WE WANT TO ALSO ADD QUESTIONS WITH ANSWERS
            /**
             * saveMany() requires Array of Objects while create() requires Array
             * So we r creating the questions ke Object(Array of questions object) ,in that array of objs there will be randomly generated questions objects and that objects are made with help of make() , so then we will apply each() i.e foreach to that questions ka Array object and then un har ek quesiton m randomly answers dalenge
             */
            factory(\App\User::class, 5)->create()
            ->each(function ($user)
                {
                    $user->questions()
                    ->saveMany(factory(Question::class, rand(2, 5))->make())
                    ->each(function($question){
                    $question->answers()->saveMany(factory(Answer::class, rand(2, 7))->make());
                });
            });
        });
    }
}
