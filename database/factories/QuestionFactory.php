<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'title' => rtrim($faker->sentence(rand(5,10)), '.'),
        'body' => $faker->paragraphs(rand(3, 7), true),
        'views_count' => rand(0, 10),
        // 'answers_count' => rand(0, 10), //iske liye comment niche likha h
        'votes_count' => rand(-10, 10),
    ];
    /**
     * Eloquent(Model) Event Handling
     * Events :
     *      retrived
     *      creating
     *      created
     *      updating
     *      updated
     *      saving
     *      saved
     *      deleteing
     *      deleted
     *      restoring
     *      restored
     *
     * These are the Events of Eloquent(Model)
     *
     * problem is : when we are creating the questions factory, so we dont want to manually add or give the 'answers_counts' with random value,
     * we want that whenever our answers are made related to any question, so that's question's 'answers_count' should be incremented
     * so from these events we will use the 'Created' event, bcoz we want that our answer is created then only the answers_count should be incremented
     * so to register the Eloquent event we have to register in the Model Class
     * Open The app/Answer.php
     */
});
